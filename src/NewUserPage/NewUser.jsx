import React from 'react';

import { userService, authenticationService } from '@/_services';

class NewUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: authenticationService.currentUserValue,
            users: null
        };
    }

    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }

    render() {
        const { currentUser, users } = this.state;
        return (
            <div>
                <h1>TODO : add fields</h1>
            </div>
        );
    }
}

export { NewUser };