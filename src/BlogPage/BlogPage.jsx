import React from 'react';

import { userService, authenticationService } from '@/_services';
import {Article} from './Article'

class BlogPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: authenticationService.currentUserValue,
            users: null
        };
    }

    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }

    render() {
        const { currentUser, users } = this.state;
        return (
            <div>
                <h1>Articles :</h1>
                {/* <Article/> */}

            </div>
        );
    }
}

export { BlogPage };